import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DownloadService } from './services/download.service';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  loading = false;
  formDown!: FormGroup;

  constructor(
    private service: DownloadService,
    private fb: FormBuilder,
    private toast: NgToastService,
  ){
    this.initFields();
  }

  initFields(){
    this.formDown = this.fb.group({
      url: ['', Validators.required],
    });
  }

  validacion(campo: string, validacion: string) {
    return (
      this.formDown.get(campo)?.hasError(validacion) &&
      this.formDown.get(campo)!.touched
    );
  }

  downloadVideo(){
    let data = this.formDown.value;
    this.loading = true;
    this.toast.success({
      detail: 'Mensaje',
      summary: 'Descargando....',
      duration: 5000,
    });
    this.service.downloadVideo(data).subscribe({
      next: (resp) => {
        if (resp.msg == 'Descarga completa.'){
          this.toast.success({
            detail: 'Mensaje',
            summary: resp.msg,
            duration: 5000,
          });
          this.initFields();
        }else{
          this.toast.warning({
            detail: 'Mensaje',
            summary: resp.msg,
            duration: 5000,
          });
        }
        this.loading = false;
      },
      error: (err) => {
        this.toast.error({
          detail: 'Mensaje',
          summary: 'Ups.. hubo un problema en la pagina',
          duration: 5000,
        });
        console.log("ERROR: " + err);
        this.loading = false;
      },
    });
  }
}
